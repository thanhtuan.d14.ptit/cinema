package cinema.manager;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;

public class Utils {

	public static Boolean validateOject(List<Object> list){
		for(Object entity: list){
			if(entity==null||entity.toString().equals("")) return false;
		}
		return true;
	}
	
	public static String createPhotoDirecroty(String photoPath) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String directory = dateFormat.format(date).replace("/", File.separator) + File.separator;
		File file = new File(photoPath + directory);
        if (!file.exists()) {
            file.mkdirs();
        }
		
		return directory;
	}
	
public static void decodeBase64Image(String imageString, String type, String absoluteFileName, int targetWidth, int targetHeight) throws IOException {
		
		// tokenize the data
		String[] tmp = imageString.split(",");
		if (tmp.length == 2) {
			imageString = tmp[1];
		} 

	   // create a buffered image
	   BufferedImage image = null;
	   byte[] imageByte;

	   imageByte = Base64.decodeBase64(imageString);
	   
	   ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
	   image = ImageIO.read(bis);

	   // Resize size
	   BufferedImage scaledImg = Scalr.resize(image, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH,
               targetWidth, targetHeight, Scalr.OP_ANTIALIAS);
	   // write the image to a file
	   File outputfile = new File(absoluteFileName);
	   if (targetHeight == 0 || targetWidth == 0) {
		   FileUtils.writeByteArrayToFile(outputfile, imageByte);
	   } else {
		   ImageIO.write(scaledImg, type, outputfile);
	   }
	   
	   bis.close();
	}
}
