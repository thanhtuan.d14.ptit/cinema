package cinema.manager.controllers;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.entities.ResponseData;
import cinema.manager.entities.User;
import cinema.manager.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping("/login")
	public ResponseData login(@RequestBody JSONObject user){
		User response = userService.loginUser(user.get("username").toString().trim(), user.get("password").toString().trim());
		if(response!=null) return new ResponseData(response, null, null, HttpStatus.OK, null);
		return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, null);
	}
}
