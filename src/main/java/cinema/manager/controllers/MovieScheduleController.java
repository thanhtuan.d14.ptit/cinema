package cinema.manager.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.Utils;
import cinema.manager.entities.Film;
import cinema.manager.entities.MovieSchedule;
import cinema.manager.entities.ResponseData;
import cinema.manager.services.FilmService;
import cinema.manager.services.MovieScheduleService;

@RestController
@RequestMapping("/movie-schedule")
public class MovieScheduleController {

	@Autowired
	private MovieScheduleService movieScheduleService;
	
	@Autowired
	private FilmService filmService;
	
	@PostMapping("/save")
	public ResponseData saveMovieSchedule(@RequestBody MovieSchedule movieSchedule){
  		List<Object> list  = new ArrayList<>();
		list.add(movieSchedule.getFilmId());
		list.add(movieSchedule.getRoom());
		list.add(movieSchedule.getShowTime());
		if(!Utils.validateOject(list))
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, "This data is invalid");
		if(movieScheduleService.checkSchedule(movieSchedule))
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, "Show time is already exist!");
		movieScheduleService.saveMovieSchedule(movieSchedule);
		return new ResponseData(null, null, null, HttpStatus.OK, null);
	}
	
	@GetMapping("/list")
	public ResponseData listShowTimeByFilm(@RequestParam("filmId")Integer filmId){
		Film film = filmService.getFilm(filmId);
		List<Object> showTime = movieScheduleService.listShowTimeByFilm(filmId);
		Map<String, Object> response = new HashMap<>();
		response.put("Film", film);
		response.put("showTime", showTime);
		return new ResponseData(response, null, null, HttpStatus.OK, null);
	}
	
	@GetMapping("/delete")
	public ResponseData deleteFilm(@RequestParam(value="ids",required=true)List<Integer> ids){
		try {
			for(Integer id :ids){
				movieScheduleService.deleteMovieSchedule(id);
			}
			return new ResponseData(null, null, null, HttpStatus.OK, null);
		} catch (Exception e) {
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
	}
	
	@GetMapping("/detail")
	public ResponseData getFilm(@RequestParam(value="id",required=true)Integer id){
		MovieSchedule film = movieScheduleService.getFilmById(id);
		if(film!=null)
			return new ResponseData(film, null, null, HttpStatus.OK, null);
		return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, "Empty.");
	}
}
