package cinema.manager.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.Configurations;
import cinema.manager.Utils;
import cinema.manager.entities.Film;
import cinema.manager.entities.ResponseData;
import cinema.manager.services.FilmService;

@RestController
@RequestMapping("/film")
public class FilmController {

	@Autowired
	private FilmService filmService;
	
	@Autowired
	private Configurations config;
	
	@PostMapping("/list")
	public ResponseData searchFilm(@RequestBody JSONObject object){
		try {
			PageRequest request = new PageRequest(Integer.parseInt(object.get("page").toString()), Integer.parseInt(object.get("size").toString()));
			String name = object.get("filmName").toString();
			String showTime = object.get("showTime").toString();
			Timestamp time = null;
			if(!showTime.equals("")) {
				LocalDateTime dateTime = LocalDateTime.parse(showTime);
				time = Timestamp.valueOf(dateTime);
			}
			Page<Film> film  = filmService.searchFilm(time, name, request);
			return new ResponseData(film.getContent(), film.getTotalPages(), film.getTotalElements(), HttpStatus.OK, null);
		}  catch (Exception e) {
			e.getStackTrace();
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, e.toString());
		}
	}
	
	@GetMapping("/checkSchedule")
	public Boolean check(@RequestParam("id")Integer filmId){
		return filmService.checkFilm(filmId);
	}
	
	@PostMapping("/save")
	public ResponseData save(@RequestBody Film film){
		
		try {
			
			if (!film.getPoster().substring(film.getPoster().length()-4, film.getPoster().length()).equals(".jpg")) {
				// Add new photo
				String photoName = System.currentTimeMillis() + "_" + UUID.randomUUID() + "." + config.getPhotoType();
				
				String absolutPath = config.getPhotoRootPath() + photoName;
				
				// Write image to disk
				Utils.decodeBase64Image(film.getPoster(), config.getPhotoType(), absolutPath, 450, 600);
				
				// Set poster for store
				film.setPoster(photoName);
			}
			
			filmService.save(film);
			
			return new ResponseData(null, null, null, HttpStatus.OK, null);
		} catch (Exception e) {
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@GetMapping("/delete")
	public ResponseData deleteFilm(@RequestParam(value="ids",required=true)List<Integer> ids){
		try {
			for(Integer id :ids){
				filmService.deleteFilmById(id);
			}
			return new ResponseData(null, null, null, HttpStatus.OK, null);
		} catch (Exception e) {
			return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
	}
	
	@GetMapping("/detail")
	public ResponseData getFilm(@RequestParam(value="id",required=true)Integer id){
		Film film = filmService.getFilm(id);
		if(film!=null)
			return new ResponseData(film, null, null, HttpStatus.OK, null);
		return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, "Empty.");
	}
}
