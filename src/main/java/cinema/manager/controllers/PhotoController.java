package cinema.manager.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.Configurations;


@RestController
@RequestMapping("/photo")
public class PhotoController {

	@Autowired
	private Configurations config;

	@RequestMapping(value = "/load/{fileName}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] loadImage(@PathVariable String fileName) {
		// Get file name
		if (fileName == null || fileName.equals("")) {
			return null;
		}
		
		try {
			// Get photo by file name
			String photoName = fileName + "." + config.getPhotoType();
			String filePath = config.getPhotoRootPath().trim() + photoName.trim();
			File file = new File(filePath); 
			InputStream is = new FileInputStream(file);
			// Prepare buffered image.
	        BufferedImage img = ImageIO.read(is);
	        // Create a byte array output stream.
	        ByteArrayOutputStream bao = new ByteArrayOutputStream();

	        // Write to output stream
	        ImageIO.write(img, config.getPhotoType(), bao);
	        
	        return bao.toByteArray();
		} catch (IOException e) {
			ExceptionUtils.getRootCauseStackTrace(e);
		}
		
		return null;
	}
}
