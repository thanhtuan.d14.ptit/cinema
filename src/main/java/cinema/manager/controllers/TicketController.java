package cinema.manager.controllers;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.entities.ResponseData;
import cinema.manager.entities.Ticket;
import cinema.manager.services.TicketService;

@RestController
@RequestMapping("/ticket")
public class TicketController {
	
	@Autowired
	private TicketService ticketService;
	
	@GetMapping("/seatNumber")
	public ResponseData listSeatNumberByFilm(@RequestParam("movie-schedule") Integer filmId){
		List<String> list = ticketService.listSeatNumberByFilm(filmId);
		return new ResponseData(list, null, (long) list.size(), HttpStatus.OK, null);
	}
	
	@PostMapping("/save")
	public ResponseData save(@RequestBody JSONObject object){
		Integer movieScheduleId = (int) object.get("movieSchedule");
		
		String seats = object.get("seats").toString();
		if(seats.startsWith("[")) 
			seats = seats.substring(1, seats.length()-1);
		String[] seat = seats.split(",");
		for(String st : seat){
			Integer a = Integer.parseInt(st.trim());
			Ticket ticket = new Ticket();
			ticket.setMovieScheduleId(movieScheduleId);
			ticket.setSeatNumber(a);
			if(!ticketService.checkTicket(ticket)) return new ResponseData(null, null, null, HttpStatus.BAD_REQUEST, "Wrong Data");
			ticketService.saveTicket(ticket);
		}
		return new ResponseData(null, null, null, HttpStatus.OK, null);
	}
}
