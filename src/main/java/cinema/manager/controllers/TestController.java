package cinema.manager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.manager.Configurations;

@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private Configurations config;

	@RequestMapping("/change")
	public String test(@RequestParam(value="text",required=false)String text){
		if(text!=null) config.setTest(text);
		return config.getTest();
	}
}
