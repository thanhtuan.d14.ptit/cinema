package cinema.manager.repositories;

import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cinema.manager.entities.Film;


public interface FilmRepository extends JpaRepository<Film, Integer> {

	public Page<Film> findByNameLikeIgnoreCase(String name, Pageable request);
	
	@Query(value="select film.* from movie_schedule inner join film on movie_schedule.film_id=film.id where movie_schedule.show_time > ?1 ORDER BY ?#{#pageable}",
			countQuery="select count(*) from movie_schedule inner join film on movie_schedule.film_id=film.id where movie_schedule.show_time > ?1 ", nativeQuery=true)
	Page<Film> findFilmByShowTimeAfter(Timestamp showTime, Pageable request);
	
	@Query(value="select film.* from movie_schedule inner join film on movie_schedule.film_id=film.id where (movie_schedule.show_time > ?1) and (LOWER(name) like lower(concat('%', ?2,'%'))) ORDER BY ?#{#pageable}",
			countQuery="select count(*) from movie_schedule inner join film on movie_schedule.film_id=film.id where (movie_schedule.show_time > ?1) and (LOWER(name) like lower(concat('%', ?2,'%')))", nativeQuery=true)
	Page<Film> findFilmByShowTimeAfterAndFilmName(Timestamp showTime, String filmName, Pageable reuquest);
}
