package cinema.manager.repositories;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cinema.manager.entities.MovieSchedule;

public interface MovieScheduleRepository extends JpaRepository<MovieSchedule, Integer> {

	@Query("select movie.id, movie.showTime from MovieSchedule movie where movie.filmId=?1")
	public List<Object> findByFilmId(Integer filmId); 
	
	public MovieSchedule findByFilmIdAndShowTime(Integer filmId, Timestamp showTime);
}
