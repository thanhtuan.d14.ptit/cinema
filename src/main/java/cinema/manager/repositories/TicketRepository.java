package cinema.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cinema.manager.entities.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {

	@Query(value="select ticket.seat_number from ticket where ticket.movie_schedule_id=?1",nativeQuery=true)
	List<String> findSeatNumberByMovieScheduleId(Integer movieScheduleId);
	
	Ticket findByMovieScheduleIdAndSeatNumber(Integer movieScheduleId, Integer seatNumber);
}
