package cinema.manager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.manager.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByUsernameAndPassword(String username, String password);
}
