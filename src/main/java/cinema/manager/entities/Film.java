package cinema.manager.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="film")
public class Film implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@GeneratedValue
	@Id
	private int id;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	private Double imdb;
	
	@Column
	private String national;
	
	@Column
	private Date releaseDate;
	
	@Column 
	private Time duration;

	@Column
	private String poster;
	
	@JsonIgnore
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL,mappedBy="filmId")
	private List<MovieSchedule> movieSchedules;
	
	
	public List<MovieSchedule> getMovieSchedules() {
		return movieSchedules;
	}

	public void setMovieSchedules(List<MovieSchedule> movieSchedules) {
		this.movieSchedules = movieSchedules;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getImdb() {
		return imdb;
	}

	public void setImdb(Double imdb) {
		this.imdb = imdb;
	}

	public String getNational() {
		return national;
	}

	public void setNational(String national) {
		this.national = national;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}
	
}
