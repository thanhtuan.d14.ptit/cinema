package cinema.manager.services;

import java.util.List;

import cinema.manager.entities.Ticket;

public interface TicketService {

	public Ticket saveTicket(Ticket ticket);
	
	public List<String> listSeatNumberByFilm(Integer filmId);
	
	public Boolean checkTicket(Ticket ticket);
}
