package cinema.manager.services;

import java.util.List;

import cinema.manager.entities.MovieSchedule;

public interface MovieScheduleService {
	
	public MovieSchedule saveMovieSchedule(MovieSchedule movie);
	
	public List<Object>  listShowTimeByFilm(Integer filmId);
	
	public MovieSchedule getFilmById(Integer id);
	
	public void deleteMovieSchedule(Integer id);
	
	public Boolean checkSchedule(MovieSchedule movieSchedule);
}
