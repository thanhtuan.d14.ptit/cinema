package cinema.manager.services.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import cinema.manager.entities.Film;
import cinema.manager.repositories.FilmRepository;
import cinema.manager.repositories.MovieScheduleRepository;
import cinema.manager.services.FilmService;

@Service
public class FilmServiceImpl implements FilmService{

	@Autowired
	private FilmRepository filmRepository;
	
	@Autowired
	private MovieScheduleRepository movieRepository;
	
	@Override
	public Page<Film> searchFilm(Timestamp time, String name, PageRequest request) {
		if(name!=null&&!name.equals("")&&time!=null)
			return filmRepository.findFilmByShowTimeAfterAndFilmName(time, name, request);
		if(name!=null&&!name.equals("")) 
			return filmRepository.findByNameLikeIgnoreCase("%"+name+"%", request);
		if(time!=null)
			return filmRepository.findFilmByShowTimeAfter(time, request);
		return filmRepository.findAll(request);
	}

	@Override
	public Film save(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public Boolean checkFilm(Integer filmId) {
		List<Object> reponse = movieRepository.findByFilmId(filmId);
		if(reponse.isEmpty()) return false;
		return true;
	}

	@Override
	public Film getFilm(Integer filmId) {
		return filmRepository.findOne(filmId);
	}

	@Override
	public void deleteFilmById(Integer id) {
		filmRepository.delete(id);
	}



}
