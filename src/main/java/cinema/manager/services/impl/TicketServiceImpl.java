package cinema.manager.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.manager.entities.Ticket;
import cinema.manager.repositories.TicketRepository;
import cinema.manager.services.TicketService;

@Service
public class TicketServiceImpl implements TicketService {

	@Autowired
	private TicketRepository ticketRepository;
	
	@Override
	public Ticket saveTicket(Ticket ticket) {
		return ticketRepository.save(ticket);
	}

	@Override
	public List<String> listSeatNumberByFilm(Integer filmId) {
		return ticketRepository.findSeatNumberByMovieScheduleId(filmId);
	}

	@Override
	public Boolean checkTicket(Ticket ticket) {
		if(ticketRepository.findByMovieScheduleIdAndSeatNumber(ticket.getMovieScheduleId(), ticket.getSeatNumber()) ==null) return true;
		return false;
	}

}
