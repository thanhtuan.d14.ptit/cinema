package cinema.manager.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.manager.entities.User;
import cinema.manager.repositories.UserRepository;
import cinema.manager.services.UserService;

@Service
public class UserServieImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User loginUser(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
		
	}

}
