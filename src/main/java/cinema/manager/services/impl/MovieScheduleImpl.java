package cinema.manager.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.manager.entities.MovieSchedule;
import cinema.manager.repositories.MovieScheduleRepository;
import cinema.manager.services.MovieScheduleService;

@Service
public class MovieScheduleImpl implements MovieScheduleService {

	@Autowired
	private MovieScheduleRepository movieScheduleRepository;
	
	@Override
	public MovieSchedule saveMovieSchedule(MovieSchedule movie) {
		return movieScheduleRepository.save(movie);
	}

	@Override
	public List<Object> listShowTimeByFilm(Integer filmId) {
		return movieScheduleRepository.findByFilmId(filmId);
	}

	@Override
	public MovieSchedule getFilmById(Integer id) {
		return movieScheduleRepository.findOne(id);
	}

	@Override
	public void deleteMovieSchedule(Integer id) {
		movieScheduleRepository.delete(id);
	}

	@Override
	public Boolean checkSchedule(MovieSchedule movieSchedule) {
		MovieSchedule response = movieScheduleRepository.findByFilmIdAndShowTime(movieSchedule.getFilmId(), movieSchedule.getShowTime());
		if(response!=null) return true;
		return false;
	}


}
