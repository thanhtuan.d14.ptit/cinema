package cinema.manager.services;

import cinema.manager.entities.User;

public interface UserService {

	public User loginUser(String username, String password);
}
