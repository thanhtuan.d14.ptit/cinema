package cinema.manager.services;

import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import cinema.manager.entities.Film;

public interface FilmService {
	
	public Page<Film> searchFilm(Timestamp time, String name, PageRequest request);
	
	public Film save(Film film);
	
	public Boolean checkFilm(Integer filmId);
	
	public Film getFilm(Integer filmId);
	
	public void deleteFilmById(Integer id);
}
